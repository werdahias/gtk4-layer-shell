# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/gtk4-layer-shell/issues
# Bug-Submit: https://github.com/<user>/gtk4-layer-shell/issues/new
# Changelog: https://github.com/<user>/gtk4-layer-shell/blob/master/CHANGES
# Documentation: https://github.com/<user>/gtk4-layer-shell/wiki
# Repository-Browse: https://github.com/<user>/gtk4-layer-shell
# Repository: https://github.com/<user>/gtk4-layer-shell.git
